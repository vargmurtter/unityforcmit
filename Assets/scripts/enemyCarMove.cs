﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyCarMove : MonoBehaviour {

    public float speed = 5f;
	
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            speed += 0.2f;
        }
        transform.Translate(
            new Vector3(1, 0, 0) * speed * Time.deltaTime);
	}
}
