﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carMove : MonoBehaviour {

    public float speed = 10f;
    Vector3 pos;
	
	void Start () {
        pos = transform.position;
	}
	
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            pos.y += 
                Input.GetAxis("Vertical") 
                * speed * Time.deltaTime;
            pos.y = Mathf.Clamp(pos.y, -3.6f, 4f);
            transform.position = pos;
        }
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            Application.LoadLevel(
                Application.loadedLevel);


            Debug.Log("TEST");
        }

        
    }
}
