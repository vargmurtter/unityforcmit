﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trackMove : MonoBehaviour {

    Vector2 offset;
    Renderer rend;
    public float speed = 0.5f;

	void Start () {
        rend = GetComponent<Renderer>();
	}
	
	void Update () {
        if (Input.GetKey(KeyCode.Space)){
            offset = new Vector2(Time.time * speed, 0);
            rend.material.mainTextureOffset = offset;
        }
	}
}
