﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour {

    public GameObject[] cars;
    public int carNum;
    public float delayTimer = 1.5f;
    private float timer;

	void Start () {
        timer = delayTimer;
	}
	
	void Update () {
        timer -= Time.deltaTime;

        if(timer <= 0)
        {
            Vector3 CarPos = new Vector3(
                transform.position.x, Random.Range(-3.7f, 3.7f),
                transform.position.z);
            carNum = Random.Range(0, 1);
            Instantiate(cars[carNum], CarPos, transform.rotation);

            timer = delayTimer;
        }
	}
}
